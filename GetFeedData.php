<?php
require "GroundController.php";
$conn = mysqli_connect($servername, $username, $password, $dbname);
$conn->set_charset("utf8");

$res = $conn->query("SELECT * FROM GroundFeedData");
$result = array();
/*
while ($row = mysqli_fetch_array($res)) {
  array_push($result, array('UserName' => $row[0], 'UserCode' => $row[1]
, 'FeedContext' => $row[2], 'CommentCount' => $row[3],
 'FeelCount' => $row[4], 'FeedTime' => $row[5], 'UserImage' => $row[6],
 'FeedCode' => $row[7], 'FeedNotice' => $row[8]));
}
*/
while ($row = mysqli_fetch_assoc($res)) {
  array_push($result, $row);
}

echo json_encode(array("result" => $result), JSON_UNESCAPED_UNICODE);

mysqli_close($conn);
 ?>
